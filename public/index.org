#+TITLE:A quantitative approach of the slake test for assessing soil structural stabilty
#+AUTHOR: Frédéric Vanwindekens, Brieuc Hardy, Christian Roisin, Fabienne Delporte
#+email: f.vanwindekens@cra.wallonie.be, b.hardy@cra.wallonie.be, f.delporte@cra.wallonie.be
#+Date: EGU2020 - Worlwide
# Sharing slides : 
# [[https://frdvnw.gitlab.io/egu2020]]


#+STARTUP: overview
#+STARTUP: hidestars
#+STARTUP: logdone
#+COLUMNS: %38ITEM(Details) %7TODO(To Do) %TAGS(Context) 
#+OPTIONS: tags:t 
#+OPTIONS: timestamp:t p:t
#+OPTIONS: todo:t
#+OPTIONS: TeX:t
#+OPTIONS: LaTeX:t          
#+OPTIONS: skip:t @:t ::t |:t ^:t f:t
#+OPTIONS: reveal_single_file:t
#+REVEAL_INIT_OPTIONS: width:1200, height:800, controlsLayout: 'bottom-right', slideNumber: true, transition:'fade'

# Comment for LaTeX
#+OPTIONS: num:nil toc:nil
#+EXCLUDE_TAGS: comments

#+REVEAL_MARGIN: 0.1
#+REVEAL_MIN_SCALE: 0.5
#+REVEAL_MAX_SCALE: 2.5
#+REVEAL_TRANS: fade
#+REVEAL_EXTRA_CSS: ./frd_slides.css
#+REVEAL_PLUGINS: (highlight markdown notes)
# +PROPERTY: results output
# +PROPERTY: exports both

#+LaTeX_CLASS: koma-article
# #+LATEX_HEADER: \usepackage[margin=0.5in]{geometry}

#+PROPERTY: header-args:R :exports both :session *R-intersoil* :tangle yes

#+REVEAL_HEAD_PREAMBLE: <meta name="description" content="A quantitative approach of the slake test for assessing soil structural stabilty">
#+REVEAL_POSTAMBLE: <p> Created by FrdVnw </p>

#+latex_header: \input{preambule.tex}
#+latex: \par Slides available here \url{https://frdvnw.gitlab.io/egu2020/}

* Introduction
** Farming practices have impacts on ecosytems ...
    [[./images/coulee_boueuse.jpg]]
    #+ATTR_HTML: :style font-size:50% ; text-align:center
    Fig. Land slides
    #+ATTR_HTML: :style font-size:20% ; text-align:right
    Credit ©RTBF, source : www.rtbf.be/info/regions/liege/detail_les-agriculteurs-souvent-pointes-du-doigt-apres-les-coulees-de-boue?id=9929773

** ... & global changes have impact on farming practices
    [[./images/secheresse2018.jpg]]
    #+ATTR_HTML: :style font-size:50% ; text-align:center
    Fig. Potatoes & drought (2018)
    #+ATTR_HTML: :style font-size:20% ; text-align:right
    Credit ©Le Sillon Belge, source : www.sillonbelge.be/2898/article/2018-08-09/rendement-et-qualite-des-pommes-de-terre-la-secheresse-et-la-canicule-pesent

** Indicators are needed, pragmatic, visual and simple for 
- assessing soil fertility, quality, health
- assessing the impact of farming practices and ecological factors on soil parameters

* Slake Test
** SlakeTest

#+REVEAL_HTML: <p><iframe width="560" height="315" src="https://www.youtube.com/embed/5UfnbiBo-Ds?start=37" frameborder="0" allowfullscreen></iframe></p>
 
** QuantiSlakeTest : a new approach for assessing *structural stability* of soils
   # +ATTR_LATEX: :width 0.5\linewidth
   # +ATTR_HTML: :width 33% :height 33%
   [[./images/schema-expe.png]]

** QuantiSlakeTest : a new approach for assessing *structural stability* of soils
    :PROPERTIES:
    :REVEAL_DATA_TRANSITION: fade
    :END:

    #+ATTR_HTML: :style font-size:40% ; text-align:right
    #+REVEAL_HTML: <p> <iframe data-autoplay width="1120" height="630" src="http://www.youtube.com/embed/G9UweThvHYI"></iframe></p>
    =http://www.youtube.com/embed/G9UweThvHYI=

** First results from 2017 promising for comparing contrasted soil management practices
    :PROPERTIES:
    :REVEAL_DATA_TRANSITION: fade
    :END:

    [[./images/slake_comp_cdm.png]]

** Confirmed by new field campaigns in 2018 et 2019 (tillage / non tillage)
    :PROPERTIES:
    :REVEAL_DATA_TRANSITION: fade
    :END:

    [[./images/slake_lf2019.png]]

** and comparison to new agroecosystems : grassland 
    :PROPERTIES:
    :REVEAL_DATA_TRANSITION: fade
    :END:

   [[./images/slake_v2019.png]]

** and comparison of other practices (organic matters)
    :PROPERTIES:
    :REVEAL_DATA_TRANSITION: fade
    :END:

   [[./images/slake_lt2019.png]]


** Identification of curves indicators
    :PROPERTIES:
    :REVEAL_DATA_TRANSITION: fade
    :END:

   [[./images/indic_slake_0409.png]]
   
** Indicators for comparing practices 
    :PROPERTIES:
    :REVEAL_DATA_TRANSITION: fade
    :END:

    [[./images/indic_comp_lt2019SL30.png]]

** In practice, the approach is opensource : 
   
    #+ATTR_HTML: :style font-size:80% 
Everything is developed throug R functions in an draft R-package including a shiny app : 
    #+ATTR_HTML: :style font-size:80% 
- for encoding the samples, campaigns, ...
- for launching a slake test by capturing data from a digital scale connected to the computer
- for visualizing the curves of carried out slake tests and controling graphical parameters (time scale, colors by modalities,...)
sources : https://frdvnw.gitlab.io/slaker/dev/


* Discussion and perspectives
** Discussion and perspectives

- quantification of a visual test
- low tech
- close to natural conditions
- highlgy contrasted for comparing some practices (tillage/conservation agriculture)
- exploration of the physical meaning of the parameters of the curves and the influence of ecological variables (type of soil, humidity, ...)
